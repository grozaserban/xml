﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BooksLibrary;
using DemoWithXML.Models;
using System.Net;

namespace DemoWithXML.Controllers
{
    [Authorize]
    public class BookController : Controller
    {
       

        private FileInterface<Book> adapter;

        public void InitXMLAdapter()
        {
            Factory factory= new Factory();
            adapter=factory.GetInterface(AdapterType.XML); 
        }
            // GET: Book
        public ActionResult Index()
        {
            InitXMLAdapter();
            var data = adapter.Read();       
            return View(data.ToList());
        }

        // GET: Books/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Books/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Title,Author,Genre,Quantity,Price")] Book book)
        {
            InitXMLAdapter();
            if (ModelState.IsValid)
            {
               
                var data = adapter.Read();
                Book last = data.FindLast(x => true);
                book.ID = last.ID + 1;
                data.Add(book);
                adapter.WriteFile(data);
                return RedirectToAction("Index");
            }

            return View(book);
        }

        // GET: Books/Edit/5
        public ActionResult Edit(int? id)
        {
            InitXMLAdapter();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var data = adapter.Read();
            Book book = data.Find(x => x.ID == id);

            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }
        // POST: Books/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,Title,Author,Genre,Quantity,Price")] Book book)
        {
            InitXMLAdapter();
            if (ModelState.IsValid)
            {
                var data = adapter.Read();
                Book old=data.Find(x => x.ID == book.ID);
                data.Remove(old);
                data.Add(book);
                adapter.WriteFile(data);
                return RedirectToAction("Index");
            }
            return View(book);
        }

        // GET: Books/Delete/5
        public ActionResult Delete(int? id)
        {
            InitXMLAdapter();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var data = adapter.Read();
            Book book = data.Find(x => x.ID == id);
            if (book == null)
            {
                return HttpNotFound();
            }
            return View(book);
        }

        // POST: Books/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            InitXMLAdapter();
            var data = adapter.Read();
            Book old = data.Find(x => x.ID == id);
            data.Remove(old);
            adapter.WriteFile(data);
            return RedirectToAction("Index");
        }

    }
}