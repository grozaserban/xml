﻿using BooksLibrary.ExternalModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Xml;

namespace BooksLibrary
{
    public class XMLadapter:XMLFactory<Book>
    {

        public List<Book> Read()
        {
            string xmlData = HttpContext.Current.Server.MapPath("~/App_Data/Books2.xml");
            DataSet ds = new DataSet();
            ds.ReadXml(xmlData);
           
            var books = new List<Book>();
            books = (from rows in ds.Tables[0].AsEnumerable()
                     select new Book
                     {
                         ID = Convert.ToInt32(rows[0].ToString()),
                         Title = rows[1].ToString(),
                         Author = rows[2].ToString(),
                         Genre = rows[3].ToString(),
                         Quantity = Convert.ToInt32(rows[4].ToString()),
                         Price = Convert.ToDecimal(rows[5].ToString()),
                     }).ToList();
            return books;
        }

        public void WriteFile(List<Book> data)
        {
            var orderedbooks=data.OrderBy(x => x.ID);
            using (XmlWriter writer = XmlWriter.Create(HttpContext.Current.Server.MapPath("~/App_Data/Books2.xml")))
	        {
	            writer.WriteStartDocument();
	            writer.WriteStartElement("Books");
                writer.WriteString("\n\n");
	            foreach (Book book in orderedbooks)
	            {
		        writer.WriteStartElement("Book");
                writer.WriteString("\n");
		        writer.WriteElementString("ID", book.ID.ToString());
                writer.WriteString("\n");
		        writer.WriteElementString("Title", book.Title);
                writer.WriteString("\n");
		        writer.WriteElementString("Author", book.Author);
                writer.WriteString("\n");
		        writer.WriteElementString("Genre", book.Genre);
                writer.WriteString("\n");
                writer.WriteElementString("Quantity", book.Quantity.ToString());
                writer.WriteString("\n");
                writer.WriteElementString("Price", book.Price.ToString());
                writer.WriteString("\n");

		        writer.WriteEndElement();
                writer.WriteString("\n\n");
	            }

	                writer.WriteEndElement();
	                writer.WriteEndDocument();
	            }
            }
        }

}
