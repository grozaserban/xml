﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BooksLibrary
{
    public interface FileInterface<T>
    {
        List<T> Read();
        void WriteFile(List<T> data);
    }
}
